import sys, os, re, time, json
import requests
import undetected_chromedriver as uc
from selenium.webdriver.common.by import By
from seleniumwire import webdriver
from urllib.parse import urlparse

headers = {
    "Secret-Key": "H2jk9uKnhRmL6WPwh89zBezWvr",
    "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/120.0.0.0 Safari/537.36"
}

cookies = {
    "__cf_bm": "1IbFzjxfaRHVYqhnvb8InJQ4hPl6xpm.UMPdGf9JtFc-1704870695-1-AYvTXrdX3DsncaAnFr63seWXYrzb97AqxwigqiiSopI8Moz49KyEuXVEpJvQaTZBTCqEbcrH/UQSQMd6hM8tGbA=",
    "_ga": "GA1.1.116903407.1704869523",
    "OptanonConsent": "isGpcEnabled=0&datestamp=Wed+Jan+10+2024+15%3A04%3A08+GMT%2B0800+(%E4%B8%AD%E5%9B%BD%E6%A0%87%E5%87%86%E6%97%B6%E9%97%B4)&version=202301.1.0&isIABGlobal=false&hosts=&landingPath=NotLandingPage&groups=C0001%3A1%2CC0002%3A1%2CC0003%3A1%2CC0004%3A1&geolocation=HK%3BHCW&AwaitingReconsent=false",
    "_sp_id.9ec1": "6bb8afbd-9851-488d-a68d-340b394693d9.1704869524.1.1704870248..fd5fe3bd-c78f-4a3c-8ddf-e686ac4dc7e7..dce87c1c-5478-4915-88f7-257ba09a3838.1704869523841.152",
    "_hjAbsoluteSessionInProgress": "0",
    "_hjIncludedInSessionSample_171201": "0",
    "_hjFirstSeen": "1",
    "ab.storage.sessionId.5791d6db-4410-4ace-8814-12c903a548ba": "%7B%22g%22%3A%22f10999d8-e533-bec0-dce6-f1b852abfed6%22%2C%22e%22%3A1704872039375%2C%22c%22%3A1704869523873%2C%22l%22%3A1704870239375%7D",
    "_hjSession_171201": "eyJpZCI6ImZlMmQ3YTA5LTc3MjUtNDM1MC1hMmU2LWM4ZWMxZjdmZDM2MyIsImMiOjE3MDQ4NzAyMjcyMTEsInMiOjAsInIiOjAsInNiIjowfQ==",
    "NEXT_LOCALE": "en-US",
    "country-code-v2": "HK",
    "OptanonAlertBoxClosed": "2024-01-10T06:52:39.803Z",
    "_gid": "GA1.2.1556762225.1704869523",
    "name-change-acknowledged": "1",
    "_hjSessionUser_171201": "eyJpZCI6Ijg0YmI5Y2I0LTBjNzMtNWJlYS1hNjJlLWM2ZjI3MGQ0NzM5MyIsImNyZWF0ZWQiOjE3MDQ4NzAyMjcyMTAsImV4aXN0aW5nIjp0cnVlfQ==",
    "_fbp": "fb.1.1704869529559.1022937902",
    "g_state": "{'i_p':1704876727097,'i_l':1}",
    "cf_clearance": "3bjBZeHrw97GI1oDa7RemSjfD8aX.zuwnau5O_iHDJs-1704869524-0-2-1b2444f8.a8739161.28b4bbb-0.2.1704869524",
    "ab.storage.deviceId.5791d6db-4410-4ace-8814-12c903a548ba": "%7B%22g%22%3A%22ba116068-9dbb-30ea-8618-0b43a67af37b%22%2C%22c%22%3A1704869523874%2C%22l%22%3A1704869523874%7D",
    "_sp_ses.9ec1": "*",
    "_ga_8JE65Q40S6": "GS1.1.1704869524.1.1.1704870699.0.0.0",
    "locale": "en-US"
}

# https://www.pexels.com/en-us/api/v3/search/photos?page=6&per_page=24&query=pretty&orientation=all&size=all&color=all&seo_tags=true

urls = ['https://www.pexels.com/en-us/api/v3/search/photos?page={}&per_page=24&query=pretty female model&orientation=portrait&size=all&color=all&seo_tags=true'.format(str(i)) for i in range(1, 800)]
result = []
page = 1
for url in urls:
    print(f'========== {url} =============')
    res = requests.get(url, headers=headers, cookies=cookies)
    data = json.loads(res.text)['data']
    for item in data:
        attr = item['attributes']
        id = str(attr['id'])
        src640 = attr['image']['medium']
        src1280 = attr['image']['large']
        downloadlink = attr['image']['download_link']
        itemJson = {'id': id, 'src640': src640, 'src1280': src1280, 'downloadlink': downloadlink}
        print(itemJson)
        result.append(itemJson)

    if len(result) == 96:
        current_dir = sys.path[0]
        file_path = os.path.join(current_dir, f'raw/category/pretty/{page}.json')
        with open(file_path, "w", encoding="utf-8") as file:
            json.dump(result, file, ensure_ascii=False, indent=4)
        result.clear()
        page += 1

# fix id type
# current_dir = sys.path[0]
# for i in range(2, 101):
#     file_path = os.path.join(current_dir, f'raw/category/model/{i}.json')
#     with open(file_path, "r", encoding="utf-8") as file:
#         content = file.read()
#         data = json.loads(content)
#         for item in data:
#             item['id'] = str(item['id'])
#         with open(file_path, "w", encoding="utf-8") as file:
#             json.dump(data, file, ensure_ascii=False, indent=4)
        
