import sys, os, re, time, json
import requests
import undetected_chromedriver as uc
from selenium.webdriver.common.by import By
from seleniumwire import webdriver
from urllib.parse import urlparse
import urllib 

cookies = {
    "sessionid": ".eJxVzEEOgjAQheG7dG1Ii1OmeplmOgxShWJouzLeXWBhdP3-972Up1pGX7OsPvbqqsB0Ggy06vQ7BeKHpH1_rstduDS1xCk3XHNZ5iNs4pEmmsUvq5eZ4vT9_WEj5XGTjLVoAxDoHmHo2QXEEJAHMRbBISJo6nQYWkOG0ODlLI7RIXUSuOUdnSjdKt1k4ySp9wfp6kRV:1rKwoI:tA4eCKblGoR0pNXiTQDGj0uoFBvtj4QE0rK9VPYGV1w"
}

q = 'abstract'
apiResponse = requests.get(f'https://pixabay.com/api/?key=41604142-48388b89f55dce407df57217f&q={q}&orientation=horizontal&per_page=200&min_width=5000&min_height=3000')
list = apiResponse.json()
data = list['hits'] 
ids = [item['id'] for item in data]
print(ids)

driver = uc.Chrome()
# 打开下载地址
driver.get('https://pixabay.com/images/download/fireworks-192979.jpg')
# 设置cookie，绕过登录
driver.add_cookie({"name": "sessionid", "value": ".eJxVzEEOgjAQheG7dG1Ii1OmeplmOgxShWJouzLeXWBhdP3-972Up1pGX7OsPvbqqsB0Ggy06vQ7BeKHpH1_rstduDS1xCk3XHNZ5iNs4pEmmsUvq5eZ4vT9_WEj5XGTjLVoAxDoHmHo2QXEEJAHMRbBISJo6nQYWkOG0ODlLI7RIXUSuOUdnSjdKt1k4ySp9wfp6kRV:1rKwoI:tA4eCKblGoR0pNXiTQDGj0uoFBvtj4QE0rK9VPYGV1w"})

# 下载高清图
try:
    for id in ids:
        driver.get(f'https://pixabay.com/images/download/fireworks-{id}.jpg')
        downloadPath = driver.current_url
        print(downloadPath)
        filepath = sys.path[0] + f'/4k/{id}.jpg'
        if not os.path.exists(filepath):
            try:
                content = requests.get(downloadPath).content
            except:
                content = bytes()
            try:
                with open(filepath, 'wb') as f:
                    f.write(content)
            except:
                pass
        else:
            print(f"{id} is exist")
            pass
except:
    pass