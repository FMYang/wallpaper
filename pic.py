import sys, os, re, time, json
import requests
import undetected_chromedriver as uc
from selenium.webdriver.common.by import By
from seleniumwire import webdriver
from urllib.parse import urlparse

# api文档https://pixabay.com/api/docs/#api_support，有速率和数量限制
cookies = {
    "sessionid": ".eJxVzEEOgjAQheG7dG1Ii1OmeplmOgxShWJouzLeXWBhdP3-972Up1pGX7OsPvbqqsB0Ggy06vQ7BeKHpH1_rstduDS1xCk3XHNZ5iNs4pEmmsUvq5eZ4vT9_WEj5XGTjLVoAxDoHmHo2QXEEJAHMRbBISJo6nQYWkOG0ODlLI7RIXUSuOUdnSjdKt1k4ySp9wfp6kRV:1rKwoI:tA4eCKblGoR0pNXiTQDGj0uoFBvtj4QE0rK9VPYGV1w"
}

# task1: 高清图下载地址（pixabay.com/images/download/fireworks-8153869.jpg），需要登录
# 1280的下载地址https://cdn.pixabay.com/photo/2023/08/06/18/16/forest-8173529_1280.jpg
# 图片id为8173529，根据图片id即可下载高清图
# driver = uc.Chrome()
# # 打开下载地址
# driver.get('https://pixabay.com/images/download/fireworks-192979.jpg')
# # 设置cookie，绕过登录
# driver.add_cookie({"name": "sessionid", "value": ".eJxVzEEOgjAQheG7dG1Ii1OmeplmOgxShWJouzLeXWBhdP3-972Up1pGX7OsPvbqqsB0Ggy06vQ7BeKHpH1_rstduDS1xCk3XHNZ5iNs4pEmmsUvq5eZ4vT9_WEj5XGTjLVoAxDoHmHo2QXEEJAHMRbBISJo6nQYWkOG0ODlLI7RIXUSuOUdnSjdKt1k4ySp9wfp6kRV:1rKwoI:tA4eCKblGoR0pNXiTQDGj0uoFBvtj4QE0rK9VPYGV1w"})

# 手动登录
# time.sleep(2)
# driver.find_element(By.XPATH, "//button[contains(@class, 'loginButton')]").click()
# time.sleep(2)
# driver.find_element(By.NAME, 'login_user').send_keys('a451493485@gmail.com')
# driver.find_element(By.NAME, 'login_pass').send_keys('frank2024')
# driver.find_element(By.XPATH, "//button[contains(@class, 'loginButton') and span[contains(@class, 'label--9n8oV')]]").click()
# time.sleep(2)

# 下载高清图
# ids = [8153869, 549153, 192979]
# for id in ids:
#     driver.get(f'https://pixabay.com/images/download/fireworks-{id}.jpg')
#     downloadPath = driver.current_url
#     print(downloadPath)
#     filepath = sys.path[0] + f'/4k/{id}.jpg'
#     if not os.path.exists(filepath):
#         try:
#             content = requests.get(downloadPath).content
#         except:
#             content = bytes()
#         try:
#             with open(filepath, 'wb') as f:
#                 f.write(content)
#         except:
#             pass
#     else:
#         print(f"{id} is exist")
#         pass

# # task2: 下载1290图片，无需登录
# # 1 - 17935
# urls = ['https://pixabay.com/images/search/?order=ec&pagi={}'.format(str(i)) for i in range(301, 500)]

# # 使用undetected_chromedriver绕过Cloudflare真人校验
# driver = uc.Chrome() 

# for url in urls:
#     try:
#         print(f'============ {url} =============')
#         driver.get(url)

#         # 获取页面高度
#         page_height = driver.execute_script("return document.body.scrollHeight")

#         # 定义滚动步长和间隔时间
#         scroll_step = 500  # 每次滚动的步长
#         scroll_interval = 0.5  # 每次滚动的间隔时间（秒）

#         # 滚动到页面底部
#         for scroll in range(0, page_height, scroll_step):
#             driver.execute_script(f"window.scrollTo(0, {scroll});")
#             time.sleep(scroll_interval)

#         time.sleep(2)
#     except:
#         pass

#     elements = driver.find_elements(By.TAG_NAME, 'img')
#     imgs = []
#     for element in elements:
#         srcset = element.get_attribute('srcset')
#         urlPattern = r"(https://[^\s]+\.jpg)"
#         try:
#             urls = re.findall(urlPattern, srcset)
#             src = urls[1]
#             # src640 = urls[0]
#             # src1280 = urls[1]
#             if 'https://cdn.pixabay.com/photo' in src:
#                 print(src)
#                 imgs.append(src)
#         except:
#             pass
    
#     # 下载
#     for img in imgs:
#         parsed_url = urlparse(img)
#         filename = os.path.basename(parsed_url.path)
#         filepath = sys.path[0] + f'/resource/{filename}'
#         if not os.path.exists(filepath):
#             try:
#                 content = requests.get(img).content
#             except:
#                 content = bytes()
#             try:
#                 with open(filepath, 'wb') as f:
#                     f.write(content)
#             except:
#                 pass
#         else:
#             print(f"{filename} is exist")

# task3: 生成json
# allpage: 1 - 17935
# vertical: 1 - 3346
# horizontal: 1 - 14593
# orientation: vertical / horizontal
# &min_width=1280&min_height=800 
# https://pixabay.com/images/search/iphone%20wallpaper/?pagi=2&orientation=vertical
# urls = ['https://pixabay.com/images/search/?order=ec&pagi={}&orientation=vertical'.format(str(i)) for i in range(11, 3346)]
# christmas 92
urls = ['https://pixabay.com/images/search/plant/?pagi={}&orientation=vertical'.format(str(i)) for i in range(83, 142)]

# 使用undetected_chromedriver绕过Cloudflare真人校验

driver = uc.Chrome() 

for index, url in enumerate(urls):
    try:
        index = index + 83
        print(f'============ {url} =============')
        driver.get(url)

        # 获取页面高度
        page_height = driver.execute_script("return document.body.scrollHeight")

        # 定义滚动步长和间隔时间
        scroll_step = 500  # 每次滚动的步长
        scroll_interval = 0.5  # 每次滚动的间隔时间（秒）

        # 滚动到页面底部
        for scroll in range(0, page_height, scroll_step):
            driver.execute_script(f"window.scrollTo(0, {scroll});")
            time.sleep(scroll_interval)
    except:
        pass

    elements = driver.find_elements(By.TAG_NAME, 'img')
    data = []
    for element in elements:
        srcset = element.get_attribute('srcset')
        urlPattern = r"(https://[^\s]+\.jpg)"
        try:
            urls = re.findall(urlPattern, srcset)
            if len(urls) > 1:
                src640 = urls[0]
                src1280 = urls[1]
                if 'https://cdn.pixabay.com/photo' in src640:
                    idPattern = r"-(\d+)_"
                    id = re.findall(idPattern, src640)[0]
                    item = {'id': str(id), 'src640': src640, 'src1280': src1280}
                    print(item)
                    data.append(item)
        except:
            print('except')
            pass
    
    # save file
    current_dir = sys.path[0]
    file_path = os.path.join(current_dir, f'raw/category/plant/{index}.json')
    with open(file_path, "w", encoding="utf-8") as file:
        json.dump(data, file, ensure_ascii=False, indent=4)


# task4: add hd imagepath to json
# driver = uc.Chrome()
# # 登录
# driver.get('https://pixabay.com/')
# time.sleep(2)
# driver.find_element(By.XPATH, "//button[contains(@class, 'loginButton')]").click()
# time.sleep(2)
# driver.find_element(By.NAME, 'login_user').send_keys('a451493485@gmail.com')
# driver.find_element(By.NAME, 'login_pass').send_keys('frank2024')
# driver.find_element(By.XPATH, "//button[contains(@class, 'loginButton') and span[contains(@class, 'label--9n8oV')]]").click()
# time.sleep(2)

# # 获取高清下载地址
# fixData = []
# jsonData = []
# current_dir = sys.path[0]
# file_path = os.path.join(current_dir, f'raw/vertical/0.json')
# with open(file_path, "r", encoding="utf-8") as file:
#     content = file.read()
#     jsonData = json.loads(content)

# for item in jsonData:
#     id = item['id']
#     src640 = item['src640']
#     src1280 = item['src1280']
#     data = {'id': id, 'src640': src640, 'src1280': src1280}
#     fixData.append(data)

# with open(file_path, "w", encoding="utf-8") as file:
#     json.dump(fixData, file, ensure_ascii=False, indent=4)